package com.trainee.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trainee.project.entity.VGACard;
import com.trainee.project.repository.VGACardRepository;

@Service
public class VGACardService {
	
	@Autowired
	private VGACardRepository vgacardRepository;

	public List<VGACard> findVGACardAll() {
		return vgacardRepository.findAll();
	}

	public VGACard findVGACardById(long id) {
		Optional<VGACard> optional = vgacardRepository.findById(id);
		return optional.get();
	}

	public VGACard saveVGACard(VGACard vgacard) {
		return vgacardRepository.save(vgacard);
	}

	public void deleteVGACard(long id) {
		vgacardRepository.deleteById(id);
		
	}

	public VGACard updateVGACard(long id, VGACard vgacard) {
		Optional<VGACard> optional = vgacardRepository.findById(id);
		VGACard update = optional.get();
		update.setBrand(vgacard.getBrand());
		update.setCoreSpeed(vgacard.getCoreSpeed());
		update.setMemorySpeed(vgacard.getMemorySpeed());
		update.setPixelShader(vgacard.getPixelShader());
		update.setVram(vgacard.getVram());
		return vgacardRepository.save(update);
	}

}
