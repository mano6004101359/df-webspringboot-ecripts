package com.trainee.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trainee.project.entity.Computer;
import com.trainee.project.repository.ComputerRepository;

@Service
public class ComputerService {
	
	@Autowired
	private ComputerRepository computerRepository;

	public List<Computer> findComputerAll() {
		List<Computer> rs = computerRepository.findAll();
		return rs;
	}

	public Computer findComputerById(long id) {
		Optional<Computer> optional = computerRepository.findById(id);
		return optional.get();
	}

	public Computer saveComputer(Computer computer) {
		return computerRepository.save(computer);
	}

	public void deleteComputer(long id) {
		computerRepository.deleteById(id);
		
	}

	public Computer updateComputer(long id, Computer computer) {
		Optional<Computer> optional = computerRepository.findById(id);
		Computer update = optional.get();
		update.setMainboard(computer.getMainboard());
		update.setCpu(computer.getCpu());
		update.setVgaCard(computer.getVgaCard());
		update.setRam(computer.getRam());
		update.setComputerName(computer.getComputerName());
		return computerRepository.save(update);
	}

}
