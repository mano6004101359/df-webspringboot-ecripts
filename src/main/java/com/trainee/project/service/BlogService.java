package com.trainee.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trainee.project.entity.Blog;
import com.trainee.project.repository.BlogRepository;

@Service
public class BlogService {

	@Autowired
	private BlogRepository blogRepository;

	public List<Blog> findBlogAll() {
		List<Blog> rs = blogRepository.findAll();
		return rs;

	}

	public Blog saveBlog(Blog blog) {
		return blogRepository.save(blog);

	}

	public void deleteBlog(long id) {
		blogRepository.deleteById(id);

	}

	public Blog updateBlog(long id, Blog blog) {
		Optional<Blog> optional = blogRepository.findById(id);
		Blog update = optional.get();
		update.setTitle(blog.getTitle());
		update.setDescription(blog.getDescription());
		return blogRepository.save(update);
	}

	public Blog findBlogById(long id) {
		Optional<Blog> optional = blogRepository.findById(id);
		return optional.get();
	}
}
