package com.trainee.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trainee.project.entity.Ram;
import com.trainee.project.repository.RamRepository;

@Service
public class RamService {
	
	@Autowired
	private RamRepository ramRepository;

	public List<Ram> findRamAll() {
		return ramRepository.findAll();

	}

	public Ram findRamById(long id) {
		Optional<Ram> optional = ramRepository.findById(id);
		return optional.get();
	}

	public Ram saveRam(Ram ram) {
		return ramRepository.save(ram);
	}

	public void deleteRam(long id) {
		ramRepository.deleteById(id);		
	}

	public Ram updateRam(long id, Ram ram) {
		Optional<Ram> optional = ramRepository.findById(id);
		Ram update = optional.get();
		update.setBrand(ram.getBrand());
		update.setRamType(ram.getRamType());
		update.setRamSize(ram.getRamSize());
		return ramRepository.save(update);
	}

}
