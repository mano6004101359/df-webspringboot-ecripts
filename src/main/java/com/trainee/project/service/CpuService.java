package com.trainee.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trainee.project.entity.Cpu;
import com.trainee.project.repository.CpuRepository;

@Service
public class CpuService {
	
	@Autowired
	private CpuRepository cpuRepository;

	public List<Cpu> findCpuAll() {
		return cpuRepository.findAll();
	}

	public Cpu findCpuById(long id) {
		Optional<Cpu> optional = cpuRepository.findById(id);
		return optional.get();
	}

	public Cpu saveCpu(Cpu cpu) {
		return cpuRepository.save(cpu);
	}

	public void deleteCpu(long id) {
		cpuRepository.deleteById(id);
		
	}

	public Cpu updateCpu(long id, Cpu cpu) {
		Optional<Cpu> optional = cpuRepository.findById(id);
		Cpu update = optional.get();
		update.setBrand(cpu.getBrand());
		update.setCacheL3(cpu.getCacheL3());
		update.setClock(cpu.getClock());
		update.setTurbo(cpu.getTurbo());
		update.setMark3D(cpu.getMark3D());
		return cpuRepository.save(update);
	}

}
