package com.trainee.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trainee.project.entity.Mainboard;
import com.trainee.project.repository.MainboardRepository;

@Service
public class MainboardService {
	
	@Autowired
	private MainboardRepository mainboardRepository;

	public List<Mainboard> findMainboardAll() {
		List<Mainboard> rs = mainboardRepository.findAll();
		return rs;
	}
	
	public Mainboard findMainboardById(long id) {
		Optional<Mainboard> optional = mainboardRepository.findById(id);
		return optional.get();
	}

	public Mainboard saveMainboard(Mainboard mainboard) {
		return mainboardRepository.save(mainboard);
	}

	public Mainboard updateMainboard(long id, Mainboard mainboard) {
		Optional<Mainboard> optional = mainboardRepository.findById(id);
		Mainboard update = optional.get();
		update.setBrand(mainboard.getBrand());
		update.setCpuType(mainboard.getCpuType());
		update.setMaxMemory(mainboard.getMaxMemory());
		return mainboardRepository.save(update);
	}

	public void deleteMainboard(long id) {
		mainboardRepository.deleteById(id);
	}

	

}
