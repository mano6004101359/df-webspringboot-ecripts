package com.trainee.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trainee.project.entity.Blog;

public interface BlogRepository extends JpaRepository<Blog, Long>{

}