package com.trainee.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trainee.project.entity.Cpu;

public interface CpuRepository extends JpaRepository<Cpu, Long>{

}
