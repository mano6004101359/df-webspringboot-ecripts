package com.trainee.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trainee.project.entity.Mainboard;

public interface MainboardRepository extends JpaRepository<Mainboard, Long>{

}
