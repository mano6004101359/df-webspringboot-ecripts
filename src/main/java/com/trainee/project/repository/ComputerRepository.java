package com.trainee.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trainee.project.entity.Computer;

public interface ComputerRepository extends JpaRepository<Computer, Long>{

}
