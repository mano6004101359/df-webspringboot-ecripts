package com.trainee.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trainee.project.entity.VGACard;

public interface VGACardRepository extends JpaRepository<VGACard, Long>{

}
