package com.trainee.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trainee.project.entity.Ram;

public interface RamRepository extends JpaRepository<Ram, Long>{

}
