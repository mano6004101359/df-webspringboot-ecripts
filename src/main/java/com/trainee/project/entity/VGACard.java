package com.trainee.project.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_VGACARD")
@AttributeOverride(name = "ID", column = @Column(name = "VGACARD_ID", nullable = false))
public class VGACard {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 3000)
	private String brand;

	@Column(length = 3000)
	private String vram;

	@Column(length = 3000)
	private String pixelShader;

	@Column(length = 3000)
	private String coreSpeed;

	@Column(length = 3000)
	private String memorySpeed;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getVram() {
		return vram;
	}

	public void setVram(String vram) {
		this.vram = vram;
	}

	public String getPixelShader() {
		return pixelShader;
	}

	public void setPixelShader(String pixelShader) {
		this.pixelShader = pixelShader;
	}

	public String getCoreSpeed() {
		return coreSpeed;
	}

	public void setCoreSpeed(String coreSpeed) {
		this.coreSpeed = coreSpeed;
	}

	public String getMemorySpeed() {
		return memorySpeed;
	}

	public void setMemorySpeed(String memorySpeed) {
		this.memorySpeed = memorySpeed;
	}

	
}
