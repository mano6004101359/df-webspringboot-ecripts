package com.trainee.project.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "T_COMPUTER")
@AttributeOverride(name = "ID", column = @Column(name = "COMPUTER_ID", nullable = false))
public class Computer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	// ส่วนประกอบ
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "MAINBOARD_ID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Mainboard mainboard;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CPU_ID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Cpu cpu;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "VGACARD_ID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private VGACard vgaCard;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RAM_ID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Ram ram;

	// ชื่อคอม
	@Column(length = 300)
	private String computerName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Mainboard getMainboard() {
		return mainboard;
	}

	public void setMainboard(Mainboard mainboard) {
		this.mainboard = mainboard;
	}

	public Cpu getCpu() {
		return cpu;
	}

	public void setCpu(Cpu cpu) {
		this.cpu = cpu;
	}

	public VGACard getVgaCard() {
		return vgaCard;
	}

	public void setVgaCard(VGACard vgaCard) {
		this.vgaCard = vgaCard;
	}

	public Ram getRam() {
		return ram;
	}

	public void setRam(Ram ram) {
		this.ram = ram;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

}
