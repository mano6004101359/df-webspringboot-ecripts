package com.trainee.project.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_CPU")
@AttributeOverride(name = "ID", column = @Column(name = "CPU_ID", nullable = false))
public class Cpu {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(length = 3000)
	private String brand;

	@Column(length = 3000)
	private String clock;
	
	@Column(length = 3000)
	private String turbo;
	
	@Column(length = 3000)
	private String cacheL3;
	
	@Column(length = 3000)
	private String mark3D;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getClock() {
		return clock;
	}

	public void setClock(String clock) {
		this.clock = clock;
	}

	public String getTurbo() {
		return turbo;
	}

	public void setTurbo(String turbo) {
		this.turbo = turbo;
	}

	public String getCacheL3() {
		return cacheL3;
	}

	public void setCacheL3(String cacheL3) {
		this.cacheL3 = cacheL3;
	}

	public String getMark3D() {
		return mark3D;
	}

	public void setMark3D(String mark3d) {
		mark3D = mark3d;
	}
	
	
}
