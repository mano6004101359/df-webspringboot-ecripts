package com.trainee.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trainee.project.entity.Blog;
import com.trainee.project.service.BlogService;

@RestController
@RequestMapping("/blog")
public class BlogController {

	@Autowired
	private BlogService blogService;

	@GetMapping("/")
	public List<Blog> findBlogAll() {
		return blogService.findBlogAll();
	}
	
	@GetMapping("/{id}")
	public Blog findBlogById(@PathVariable long id) {
		return blogService.findBlogById(id);
	}

	@PostMapping("/")
	public Blog saveBlog(@RequestBody Blog blog) {
		return blogService.saveBlog(blog);
	}

	@DeleteMapping("/{id}")
	public void deleteBlog(@PathVariable long id) {
		blogService.deleteBlog(id);
	}
	
	@PutMapping("/{id}")
	public Blog updateBlog(@PathVariable long id,@RequestBody Blog blog) {
		return blogService.updateBlog(id, blog);
	}
}
