package com.trainee.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trainee.project.entity.Ram;
import com.trainee.project.service.RamService;

@RestController
@RequestMapping("/ram")
public class RamController {
	
	@Autowired
	private RamService ramService;

	@GetMapping("/")
	public List<Ram> findRamAll() {
		return ramService.findRamAll();
	}
	
	@GetMapping("/{id}")
	public Ram findRamById(@PathVariable long id) {
		return ramService.findRamById(id);
	}

	@PostMapping("/")
	public Ram saveRam(@RequestBody Ram ram) {
		return ramService.saveRam(ram);
	}

	@DeleteMapping("/{id}")
	public void deleteRam(@PathVariable long id) {
		ramService.deleteRam(id);
	}
	
	@PutMapping("/{id}")
	public Ram updateRam(@PathVariable long id,@RequestBody Ram ram) {
		return ramService.updateRam(id, ram);
	}
}
