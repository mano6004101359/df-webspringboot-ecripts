package com.trainee.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trainee.project.entity.Computer;
import com.trainee.project.service.ComputerService;

@RestController
@RequestMapping("/computer")

public class ComputerController {
	
	@Autowired
	private ComputerService computerService;

	@GetMapping("/")
	public List<Computer> findComputerAll() {
		return computerService.findComputerAll();
	}
	
	@GetMapping("/{id}")
	public Computer findComputerById(@PathVariable long id) {
		return computerService.findComputerById(id);
	}

	@PostMapping("/")
	public Computer saveComputer(@RequestBody Computer computer) {
		return computerService.saveComputer(computer);
	}

	@DeleteMapping("/{id}")
	public void deleteComputer(@PathVariable long id) {
		computerService.deleteComputer(id);
	}
	
	@PutMapping("/{id}")
	public Computer updateComputer(@PathVariable long id,@RequestBody Computer computer) {
		return computerService.updateComputer(id, computer);
	}
}
