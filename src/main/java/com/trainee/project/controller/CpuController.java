package com.trainee.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trainee.project.entity.Cpu;
import com.trainee.project.service.CpuService;

@RestController
@RequestMapping("/cpu")
public class CpuController {
	
	@Autowired
	private CpuService cpuService;

	@GetMapping("/")
	public List<Cpu> findCpuAll() {
		return cpuService.findCpuAll();
	}
	
	@GetMapping("/{id}")
	public Cpu findCpuById(@PathVariable long id) {
		return cpuService.findCpuById(id);
	}

	@PostMapping("/")
	public Cpu saveCpu(@RequestBody Cpu cpu) {
		return cpuService.saveCpu(cpu);
	}

	@DeleteMapping("/{id}")
	public void deleteCpu(@PathVariable long id) {
		cpuService.deleteCpu(id);
	}
	
	@PutMapping("/{id}")
	public Cpu updateCpu(@PathVariable long id,@RequestBody Cpu cpu) {
		return cpuService.updateCpu(id, cpu);
	}
}
