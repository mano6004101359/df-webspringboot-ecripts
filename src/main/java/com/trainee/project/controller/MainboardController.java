package com.trainee.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trainee.project.entity.Mainboard;
import com.trainee.project.service.MainboardService;

@RestController
@RequestMapping("/mainboard")
public class MainboardController {
	
	@Autowired
	private MainboardService mainboardService;

	@GetMapping("/")
	public List<Mainboard> findMainboardAll() {
		return mainboardService.findMainboardAll();
	}
	
	@GetMapping("/{id}")
	public Mainboard findMainboardById(@PathVariable long id) {
		return mainboardService.findMainboardById(id);
	}

	@PostMapping("/")
	public Mainboard saveMainboard(@RequestBody Mainboard mainboard) {
		return mainboardService.saveMainboard(mainboard);
	}

	@DeleteMapping("/{id}")
	public void deleteMainboard(@PathVariable long id) {
		mainboardService.deleteMainboard(id);
	}
	
	@PutMapping("/{id}")
	public Mainboard updateMainboard(@PathVariable long id,@RequestBody Mainboard mainboard) {
		return mainboardService.updateMainboard(id, mainboard);
	}
}
